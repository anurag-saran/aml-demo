package com.redhat.aml.pig;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.Properties;

import org.apache.pig.ExecType;
import org.apache.pig.PigServer;
import org.apache.pig.backend.executionengine.ExecException;
import org.apache.pig.data.Tuple;
import org.eclipse.jgit.api.errors.GitAPIException;

import com.redhat.aml.model.CustomerProfile;
import com.redhat.aml.pig.util.BuildUtils;
import com.redhat.aml.pig.util.GitUtils;
import com.redhat.aml.table.GuidedDecisionTableWriter;
import com.redhat.aml.table.util.DecisionTableGenerator;

public class GenerateCustomerProfile {
	private static final String PASSWORD = "abcd1234!";
	private static final String USERNAME = "bpmsAdmin";
	private static final String HOSTNAME = "localhost";
	private static final String REPOSITORY = "aml";
	private static final String PROJECT = "antimoneyLaundering";
	private static final String GIT_PORT = "8001";

	public static void main(String[] args) throws ExecException, IOException {
		Properties props = new Properties();
		props.setProperty("fs.defaultFS", "hdfs://192.168.56.101:8020");

		PigServer pigServer = new PigServer(ExecType.MAPREDUCE, props);
		String transactionsFile = "hdfs://192.168.56.101:8020/flume/transactions";
		String accountsFile = "hdfs://192.168.56.101:8020/user/hue/account.txt";
		
		//Use Pig to transform transaction and account data into various customer profiles
		Iterator<Tuple> profileTuples = runMyQuery(pigServer, transactionsFile, accountsFile);
		
		//Create destination for new decision table
		File file = new File("/tmp/custprofilelist.xml");
		
		//Create new decision table
		writeCustomerProfileDecisionTable(profileTuples, file);
		
		//Update the git repo in BRMS
		updateGitRepo(file, HOSTNAME, GIT_PORT, REPOSITORY, PROJECT, USERNAME, PASSWORD);
		
		//Build the BRMS project
		BuildUtils.install(REPOSITORY, PROJECT, HOSTNAME, USERNAME, PASSWORD);
	}

	public static Iterator<Tuple> runMyQuery(PigServer pigServer, String transactionsFile,
			String accountsFile){
		try {
			pigServer.registerQuery("transaction = load '" + transactionsFile + "' using PigStorage(',') as (TransactionID:int,AccountNo:int,Amount:int,TransactionType:chararray,FromZipCode:chararray,ToZipCode:chararray,IPAddress:chararray,DeviceLocation:chararray,Country:chararray,State:chararray);");
			pigServer.registerQuery("account = load '" + accountsFile + "' using PigStorage(',') as (AccountNo:int, FirstName:chararray, LastName:chararray, Street:chararray, City:chararray, State:chararray, ZipCode:chararray, Occupation:chararray, Age:int, Sex:chararray,MaritalStatus:chararray, AccountType:chararray);");
			pigServer.registerQuery("C = foreach account generate AccountNo as id, ZipCode,Occupation;");
			pigServer.registerQuery("jnd = join transaction by AccountNo, C by id;");
			pigServer.registerQuery("D = group jnd by (C::ZipCode,transaction::TransactionType,C::Occupation);");
			pigServer.registerQuery("E = foreach D generate flatten(group) as (zip,Tranaction,occupation),SUM($1.Amount) as total_spent,COUNT(jnd) as numOfTransactions,AVG($1.Amount) as avg, MIN($1.Amount) as min, MAX($1.Amount) as max;");
			return pigServer.openIterator("E");
		} catch (IOException e) {
			throw new IllegalArgumentException("Could not execute pig query: " , e);
		}
	}
	
	public static void writeCustomerProfileDecisionTable(Iterator<Tuple> profileTuples, File file){
		GuidedDecisionTableWriter tableWriter = new GuidedDecisionTableWriter(file,DecisionTableGenerator.getCustomerProfileDecisionTable());
		Tuple tup;
		CustomerProfile customerProfile;
		while (profileTuples.hasNext()) {
			tup = profileTuples.next();
			customerProfile = new CustomerProfile();
			try {
				customerProfile.setZipCode((String) tup.get(0));
				customerProfile.setTransactionType((String) tup.get(1));
				customerProfile.setOccupation((String) tup.get(2));
				customerProfile.setTotalSpent(Double.parseDouble(tup.get(3)
						.toString()));
				customerProfile.setNumberOfTransactions(Integer.parseInt(tup.get(4)
						.toString()));
				customerProfile.setAverage((Double.parseDouble(tup.get(5)
						.toString())));
				customerProfile.setMinimum((Double.parseDouble(tup.get(6)
						.toString())));
				customerProfile.setMaximum((Double.parseDouble(tup.get(7)
						.toString())));
				customerProfile.setStandardDeviation(15.0);
				customerProfile.setLowPenalty(10);
				customerProfile.setHighPenalty(15);
			} catch ( ExecException e) {
				System.err.println("Could not parse tuple and extract customer profile");
				e.printStackTrace(System.err);
			}
			tableWriter.writeCustomer(customerProfile);
		}
		tableWriter.close();
	}
	
	public static void updateGitRepo(File file, String hostname, String port, String repository, String project, String username, String password ) {
		GitUtils gitRepo = new GitUtils();
		try {
			String gitUrl = "ssh://" + username + "@"+hostname+":"+port+"/" + repository;
			gitRepo.cloneRepo(gitUrl, password);
			gitRepo.newDecisionTable(project, file);
			gitRepo.addCommitAndPush(password);
			
		} catch ( GitAPIException e) {
			System.err.println("Git error occured!");
			e.printStackTrace(System.err);
		} catch (IOException e) {
			System.err.println("IO error occured!");
			e.printStackTrace(System.err);
		}
	}

}