package com.redhat.aml.table.model;

import java.util.ArrayList;
import java.util.List;

public class Pattern {
	private List<ConditionColumn<?>> conditionColumns;
	private String factType;
	private String boundName;
	private Boolean isNegated = false;
	public Pattern(String factType, String boundName) {
		super();
		this.conditionColumns = new ArrayList<ConditionColumn<?>>();
		this.factType = factType;
		this.boundName = boundName;
	}
	public List<ConditionColumn<?>> getConditionColumns() {
		return conditionColumns;
	}
	public Pattern addConditionColumns(ConditionColumn<?> conditionColumn) {
		this.conditionColumns.add(conditionColumn);
		return this;
	}
	public String getFactType() {
		return factType;
	}
	public void setFactType(String factType) {
		this.factType = factType;
	}
	public String getBoundName() {
		return boundName;
	}
	public void setBoundName(String boundName) {
		this.boundName = boundName;
	}
	public Boolean getIsNegated() {
		return isNegated;
	}
	public void setIsNegated(Boolean isNegated) {
		this.isNegated = isNegated;
	}
	
	
}
