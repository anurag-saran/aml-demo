package com.redhat.aml.table.model;

public class ConditionColumn<T> extends Column{
	private DataType<T> dataType;
	private T defaultValue;
	private String header;
	private String constraintValueType = "1";
	private String factField;
	private String fieldType;
	private String operator;
	public ConditionColumn(DataType<T> dataType, T defaultValue, String header,
			String constraintValueType, String factField, String fieldType,
			String operator) {
		super();
		this.dataType = dataType;
		this.defaultValue = defaultValue;
		this.header = header;
		this.constraintValueType = constraintValueType;
		this.factField = factField;
		this.fieldType = fieldType;
		this.operator = operator;
	}
	public DataType<T> getDataType() {
		return dataType;
	}
	public T getDefaultValue() {
		return defaultValue;
	}
	public String getHeader() {
		return header;
	}
	public String getConstraintValueType() {
		return constraintValueType;
	}
	public String getFactField() {
		return factField;
	}
	public String getFieldType() {
		return fieldType;
	}
	public String getOperator() {
		return operator;
	}
}
