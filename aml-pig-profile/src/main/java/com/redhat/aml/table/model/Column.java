package com.redhat.aml.table.model;

public abstract class Column {
	private Integer width = -1;
	private Boolean hideColumn = false;
	public Integer getWidth() {
		return width;
	}
	public void setWidth(Integer width) {
		this.width = width;
	}
	public Boolean getHideColumn() {
		return hideColumn;
	}
	public void setHideColumn(Boolean hideColumn) {
		this.hideColumn = hideColumn;
	}
}
