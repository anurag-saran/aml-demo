package com.redhat.aml.table.model;

import java.util.ArrayList;
import java.util.List;

public class DecisionTable {
	private String tableFormat = "EXTENDED_ENTRY";
	private Boolean hideRowNumberColumn = false;
	private Boolean hideDescriptionColumn = false;
	private String tableName;
	private String packageName;
	private List<String> imports;
	private List<Pattern> patterns;
	private List<AttributeColumn<?>> attributeColumns;
	private List<Column> actionColumns;
	private AuditLogFilter auditLogger = new AuditLogFilter("org.drools.guvnor.client.modeldriven.dt52.auditlog.DecisionTableAuditLogFilter");
	
	
	public DecisionTable(String packageName, String tableName) {
		this.tableName = tableName;
		this.packageName = packageName;
		this.imports = new ArrayList<String>();
		this.attributeColumns = new ArrayList<AttributeColumn<?>>();
		this.actionColumns = new ArrayList<Column>();
		this.patterns = new ArrayList<Pattern>();
	}
	
	public DecisionTable addPattern(Pattern pattern) {
		this.patterns.add(pattern);
		return this;
	}
	
	public DecisionTable addImport(String importName){
		imports.add(importName);
		return this;
	}
	
	public DecisionTable addAttributeColumn(AttributeColumn<?> attributeColumn) {
		attributeColumns.add(attributeColumn);
		return this;
	}
	
	public DecisionTable addActionColumn(Column actionColumn) {
		actionColumns.add(actionColumn);
		return this;
	}
	
	public String getTableName() {
		return this.tableName;
	}

	public String getTableFormat() {
		return tableFormat;
	}

	public Boolean getHideRowNumberColumn() {
		return hideRowNumberColumn;
	}

	public Boolean getHideDescriptionColumn() {
		return hideDescriptionColumn;
	}

	public String getPackageName() {
		return packageName;
	}

	public List<String> getImports() {
		return imports;
	}
	
	public List<AttributeColumn<?>> getAttributeColumns() {
		return attributeColumns;
	}

	public AuditLogFilter getAuditLogger() {
		return auditLogger;
	}

	public List<Column> getActionColumns() {
		return actionColumns;
	}
	
	public List<Pattern> getPatterns() {
		return patterns;
	}
}
