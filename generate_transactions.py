#!/usr/bin/python
# 
# Generate transaction files
# Format: Credit Card #, City, State, Amount 

import random
import logging
import csv 
from optparse import OptionParser

def read_options():
    parser = OptionParser()
    parser.add_option("-f", "--logfile", dest="logfile", help="Specify a log file. Default=./transactions.log", default="./transactions.log", type="string")
    parser.add_option("-n", "--number-of-iterations", dest="iterations", help="Specify the number of log events to create. Default=100", default="100", type="int")
    (options, args) = parser.parse_args()
    return options

def get_amount():
    return random.randint(0,1000)

def load_accounts():
    #account id, firstname, lastname, address, city, state, zip, occupation, age, sex, married, account type
    accounts = []  
    cr = csv.reader(open("account.txt","rb"))
    for row in cr: 
        new_account = { 'id' : row[0], 'firstname' : row[1], 'lastname' : row[2], 'address' : row[3], 'city' : row[4], 'state' : row[5], 'zip' : row[6], 'occupation' : row[7], 'age' : row[8], 'sex' : row[9], 'married' : row[10], 'type' : row[11]}
        accounts.append(new_account)
    return accounts

def zipcodes():
    return [ '28226', '28206', '18821', '60091', '8820', '60090' ]

def ip_addresses():
    return ['192.168.10.123','192.168.10.13','192.168.10.223','192.168.10.124']

def transaction_types():
    return ['CR', 'DR', 'CC', 'MoneyTransfer']

def devices():
    return [ 'Bank', 'ATM', 'Food', 'iPhone' ]

def generate_transaction(t_id, account):
    transaction_amount = get_amount()
    transaction_type = random.choice(transaction_types())
    fromzip = random.choice(zipcodes())
    tozip = random.choice(zipcodes())
    ip = random.choice(ip_addresses())
    device = random.choice(devices())
    # transaction id, account id, amount, transaction type, from zip, to zip, ip, device, country, state
    return { 'id' : t_id, 'account_id' : account['id'], 'amount' : transaction_amount, 'type' : transaction_type, 'fromzip' : fromzip, 'tozip' : tozip, 'ip' : ip, 'device' : device, 'state' : account['state'], 'city' : account['city'] }

def main():
    options = read_options()
    logfile = options.logfile
    iterations = options.iterations
    logging.basicConfig(filename=options.logfile,
                        format='%(message)s',
                        level=logging.DEBUG)
    accounts = load_accounts()
    for x in range(iterations):
        account = random.choice(accounts)
        t = generate_transaction(random.randint(0,999999), account)
	# transaction id, account id, amount, transaction type, from zip, to zip, ip, device, country, state
        logging.info('%s,%s,%s,%s,%s,%s,%s,%s,%s,%s'%(t['id'],t['account_id'], t['amount'],t['type'],t['fromzip'],t['tozip'],t['ip'],t['device'],t['state'],t['city']))


if __name__ == "__main__":
    main()



