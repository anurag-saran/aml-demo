package com.redhat.aml.restservices;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.redhat.aml.model.Row;
import com.redhat.aml.model.RuleExecution;
import com.redhat.aml.model.RuleExecutionDB;
import com.redhat.aml.model.service.RuleExecutionService;
import com.redhat.aml.model.service.TransactionService;
import com.redhat.aml.model.transform.RuleExecutionTransform;

@Path("/ruleexecution")
@Stateless
public class RuleExecutionEndpoint {
	private static final Logger LOG = LoggerFactory.getLogger(RuleExecutionEndpoint.class);

	@Inject
	RuleExecutionService ruleExecutionService ;
	
	@Inject
	TransactionService transactionService;
	

	// POST http://localhost:8080/aml-frontend-services/ruleexecution
	// With Body: {"id": 1, "transactionId": 12345, "packageName": "com.redhat.antimoneylaundering", "ruleName": "Row 1 rule one"}
	@POST
	@Consumes("application/json")
	public Response create(final RuleExecution ruleExecution) {
		LOG.info("Creating " + ruleExecution);
		RuleExecutionDB entity = RuleExecutionTransform.transform(ruleExecution);
		ruleExecutionService.createRuleExecution(entity);
		
		return Response.ok().build();
	}

	// GET http://localhost:8080/aml-frontend-services/ruleexecution/11223344
	@GET
	@Path("/{id}")
	@Produces("application/json")
	public List<RuleExecution> findById(@PathParam("id") final Integer id) {
//		TransactionDB entity = transactionService.findTransaction(id);

//		if (entity == null) {
//			return Response.status(Status.NOT_FOUND).build();
//		}
		
		List<RuleExecutionDB> ruleExecutionDBs = ruleExecutionService.findRuleExecutionsByTransactionId(id);
		List<RuleExecution> toReturn = new ArrayList<RuleExecution>(ruleExecutionDBs.size());
		for (RuleExecutionDB db : ruleExecutionDBs) {
			toReturn.add(RuleExecutionTransform.transform(db));
		}
		return toReturn;
	}
	
	// GET http://localhost:8080/aml-frontend-services/ruleexecution/rows/3
	// returns [{"zipCode":"8820","occupation":"BarOwner","transactionType":"CC","limit":994,"fired":true,"name":"Row 1 CustomerProfileGDT","transactionScore":"10.0"}
	//         ,{"zipCode":"8820","occupation":"BarOwner","transactionType":"CR","limit":996,"fired":false,"name":"Row 2 CustomerProfileGDT","transactionScore":"10.0"}
    //         ....]
	@GET
	@Path("/rows/{id}")
	@Produces("application/json")
	public List<Row> findRowsId(@PathParam("id") final Integer id) {
		return ruleExecutionService.getAllRows(id);
	}

}
