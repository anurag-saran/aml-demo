package com.redhat.aml.test;
import com.redhat.aml.model.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.manager.RuntimeEngine;
import org.kie.remote.client.api.RemoteRestRuntimeEngineFactory;
import org.kie.services.client.api.command.RemoteConfiguration;
import org.kie.services.client.api.command.RemoteRuntimeEngine;

public class AmlProcessStarter {
	
	private static final String BPMS_USER = "appadmin";
	private static final String BPMS_PASSWORD = "Pass@123";
	private static final String BPMS_HOST = "localhost:8080";
	private static final String DEPLOYMENT_ID = "com.redhat:antimoneyLaundering:1.0";
	private static final String PROCESS_NAME = "antimoneyLaundering.amlProcess";
	
	public static void main(String[] args) throws MalformedURLException {
		URL baseUrl = new URL("http://" + BPMS_HOST + "/business-central");
		
		RemoteConfiguration config = new RemoteConfiguration(DEPLOYMENT_ID, baseUrl, BPMS_USER, BPMS_PASSWORD);
		config.setServerBaseRestUrl(baseUrl);
		RemoteRestRuntimeEngineFactory factory = new RemoteRestRuntimeEngineFactory(config);
        RemoteRuntimeEngine engine = factory.newRuntimeEngine();
        KieSession ksession = engine.getKieSession();

		// Call process with a name
		Map<String, Object> processVars = new HashMap();
//		AccountDB acc= new AccountDB(UUID.randomUUID().toString(),
//				"Savings", 3, "New York", "Elmo", "Monster", "s",
//				"Student", "m", "NY", "123 Sesame Street", "10001");
		
		Account acc= new Account(UUID.randomUUID().toString(),
		"Savings", 3, "New York", "Elmo", "Monster", "s",
		"Student", "m", "NY", "123 Sesame Street", "10001", "7045551234");
		processVars.put("account", acc);
		

		
//		TransactionDB trns = new TransactionDB(
//		 11,
//		  "123456789",
//		 null,
//		"CR",
//		 "60090",
//		 "08820",
//		 "11.11.11.11",
//		"deviceLoc",
//		"US",
//		 "IL",
//		 Date.valueOf("2015-08-05"),1D);
		
		Transaction trns = new Transaction(1,"12",12L,"CR","60090","08820","12.12.12.12","1212-1212","","",Date.valueOf("2015-08-05"), "");
		
		processVars.put("transaction", trns);
		ksession.startProcess(PROCESS_NAME, processVars);
	}
	
	

}