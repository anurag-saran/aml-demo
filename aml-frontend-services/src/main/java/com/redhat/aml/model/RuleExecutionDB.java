package com.redhat.aml.model;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Transaction 
 */
@Entity
@Table(name = "ruleexecution")
public class RuleExecutionDB implements java.io.Serializable {

	/** svuid */
	private static final long serialVersionUID = -1723072699124282844L;
	
	private Integer id;
	private Integer transactionId;
	private String packageName;
	private String ruleName;
	
	public RuleExecutionDB() {
		super();
	}

	public RuleExecutionDB(Integer id, Integer transactionId,
			String packageName, String ruleName) {
		super();
		this.id = id;
		this.transactionId = transactionId;
		this.packageName = packageName;
		this.ruleName = ruleName;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "transactionId", nullable = false)
	public Integer getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(Integer transactionId) {
		this.transactionId = transactionId;
	}

	@Column(name = "packagename", nullable = false)
	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	@Column(name = "rulename", nullable = false)
	public String getRuleName() {
		return ruleName;
	}

	public void setRuleName(String ruleName) {
		this.ruleName = ruleName;
	}

}
