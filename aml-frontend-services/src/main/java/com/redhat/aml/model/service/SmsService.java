package com.redhat.aml.model.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Singleton;

import org.apache.commons.lang.StringUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.redhat.aml.model.Account;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.TwilioRestException;
import com.twilio.sdk.resource.factory.MessageFactory;
import com.twilio.sdk.resource.instance.Message;

@Singleton
public class SmsService {
	private static final Logger LOG = LoggerFactory.getLogger(SmsService.class);
	private static final String ACCOUNT_SID = "AC58ab1867344ea17ba31ef4e21b577593";
	private static final String API_KEY = "5689d4915a50789da2edcbdcb4251afb";
	private static final String FROM = "+1 (704) 504-7782";

	public Message sendMessage(Account account, String smsBody) {
		String phoneNumber = account.getPhoneNumber();;
		if (StringUtils.isEmpty(phoneNumber)) {
			LOG.error("Account "+account.getAccountNo() +"has no phone number, not sending message! ");
			return null;
		}
		LOG.info("Creating Twilio client...");
		TwilioRestClient client = new TwilioRestClient(ACCOUNT_SID, API_KEY);
		 
	    // Build a filter for the MessageList
		LOG.info("Building message...");
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("Body", smsBody));
	    //replace 'to' with field from Account
	    params.add(new BasicNameValuePair("To", "+1" + phoneNumber));
	    params.add(new BasicNameValuePair("From", FROM));
	 
	    LOG.info("Getting MessageFactory...");
	    MessageFactory messageFactory = client.getAccount().getMessageFactory();
	    try {
	    	LOG.info("Sending SMS Message");
			return messageFactory.create(params);
		} catch (TwilioRestException e) {
			LOG.error("Could not send SMS Message:", e);
			return null;
		}
	}
}
