package com.redhat.aml.util;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.transport.CredentialsProvider;

public class GitUtils {
	private final File location;
	
	public GitUtils() {
		location = getLocation();
	}
	
	public void cloneRepo(String source, String password)
			throws GitAPIException, IOException {
		CredentialsProvider credentialsProvider = new GitCredentialsProvider(password);
		location.delete();
		Git result = null;
		try {
			result = Git.cloneRepository()
					.setCredentialsProvider(credentialsProvider).setURI(source)
					.setDirectory(location).setBare(false).call();
		} finally {
			if (result != null) {
				result.close();
			}
		}
	}

	public File getDecisionTable() {
		String project = "antimoneyLaundering";
		return new File(location.getPath() + "/"+project+"/src/main/resources/com/redhat/antimoneylaundering/CustomerProfileGDT.gdst");
	}
	
    private File getLocation() {
        String tempDir = System.getProperty("java.io.tmpdir") + "/" + "aml";
        return new File(tempDir, "aml-" + UUID.randomUUID().toString());
    }

}
