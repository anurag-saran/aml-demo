package com.redhat.aml.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;

@XmlRootElement(name="row")
@XmlAccessorType(XmlAccessType.FIELD)
public class Row {
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String zipCode;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String occupation;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String transactionType;
	
	@XmlElement
	@XmlSchemaType(name="int")
	private Integer limit;
	
	@XmlElement
	@XmlSchemaType(name="boolean")
	private Boolean fired;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String name;
	
	@XmlElement
	@XmlSchemaType(name="string")
	private String transactionScore;
	
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getOccupation() {
		return occupation;
	}
	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public Integer getLimit() {
		return limit;
	}
	public void setLimit(Integer limit) {
		this.limit = limit;
	}
	public Boolean getFired() {
		return fired;
	}
	public void setFired(Boolean fired) {
		this.fired = fired;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getTransactionScore() {
		return transactionScore;
	}
	public void setTransactionScore(String transactionScore) {
		this.transactionScore = transactionScore;
	}
	
}
