package com.redhat.antimoneylaundering;

import org.kie.api.event.rule.AfterMatchFiredEvent;
import org.kie.api.event.rule.DefaultAgendaEventListener;
import org.kie.api.runtime.rule.Match;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.redhat.aml.model.RuleExecution;
import com.redhat.aml.model.Transaction;
import com.redhat.antimoneylaundering.util.RESTUtil;

public class RestAgendaEventListener extends DefaultAgendaEventListener {
	private static final Logger LOG = LoggerFactory.getLogger(RestAgendaEventListener.class);

	public void afterMatchFired(AfterMatchFiredEvent event) {
		Match match = event.getMatch();
		String ruleName = match.getRule().getName();
		String packageName = match.getRule().getPackageName();
		LOG.info("Match fired for rule '"+ruleName+"' in '"+packageName+"'");
		for (String id : match.getDeclarationIds()) {
			Object value = match.getDeclarationValue(id);
			LOG.info("-" + id + ":" + value);
			if (value instanceof Transaction) {
				Transaction transaction = (Transaction) value;
				LOG.info("-^storing " + transaction);
				try {
					this.create(new RuleExecution(1, transaction.getTransactionid(), packageName, ruleName));
				} catch (Exception e) {
					LOG.error("Error persisting rule execution", e);
				}
			}
		}
	}
	
	private void create(RuleExecution ruleExecution) {
		RESTUtil.POST("/aml-frontend-services/ruleexecution/", ruleExecution);
	}
}
