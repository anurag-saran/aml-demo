package com.redhat.antimoneylaundering.handler;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.redhat.aml.model.Account;
import com.redhat.aml.model.Transaction;
import com.redhat.antimoneylaundering.util.RESTUtil;

public class NotifyFraudWorkItemHandler implements WorkItemHandler {
	private static final Logger LOG = LoggerFactory.getLogger(NotifyFraudWorkItemHandler.class);

	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
		Transaction transaction = (Transaction) workItem.getParameter("transaction");
		Account account = (Account) workItem.getParameter("account");
		LOG.info("Frad alert created for " + account + " , " + transaction);
		RESTUtil.POST("/aml-frontend-services/fraud/" + account.getAccountNo(), transaction);
		manager.completeWorkItem(workItem.getId(), null);
	}

	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		//No-OP
	}

}
